#include <string>



#ifdef __cplusplus
extern "C" {
#endif

    void hello_user(const char *name);
    void goodbye();
    
    sexp sexp_init_library (sexp ctx, sexp self, sexp_sint_t n, sexp env, const char* version, const sexp_abi_identifier_t abi);
#ifdef __cplusplus
}
#endif


struct vec2
{
    float x,y;
    static constexpr const char* kName = "vec2";
};

struct rectangle
{
    struct vec2 position;
    const char* name;
    unsigned id;
};

