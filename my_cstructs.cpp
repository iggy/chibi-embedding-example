#include <stdio.h>

void hello_user(const char *name) {
    printf("Hello, %s.\n", name);
    return;
}

void goodbye() {
    printf("Goodbye!\n");
    return;
}
