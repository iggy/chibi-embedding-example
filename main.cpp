#include <iostream>
#include <string>

#include <random>
#include <functional>
#include <vector>
#include <algorithm>
#include <iterator>
#include "chibi/eval.h"

#include "my_cstructs.h"

void printVector(const vec2& v, std::string prefix = "My vector: ")
{
    std::cout << prefix << v.x << "; " << v.y << std::endl;
}

void printRect(const rectangle& r)
{
    printVector(r.position);
}

template <typename T>
struct chibiCT
{
    
};

template <>
struct chibiCT<vec2>
{
static constexpr const char* kName = "vec2";    
};


template <typename C>
sexp buildList(sexp ctx, C& container)
{
    // Declare and preserve local variables
    sexp_gc_var3(obj1, objList, obj3);
    sexp_gc_preserve3(ctx, obj1, objList, obj3);
    
    /*
     for building a list:
     args = sexp_cons(ctx, arg_k, SEXP_NULL);
     ...
     args = sexp_cons(ctx, arg_1, args);
     args = sexp_cons(ctx, arg_0, args);
     
     */
    obj1 = sexp_c_string(ctx, chibiCT<typename C::value_type>::kName, -1);
    obj1 = sexp_lookup_type(ctx, obj1, SEXP_FALSE);
    auto typeTag = sexp_type_tag(obj1);
    
    // We convert from the back as 'cons' will add new elements from the beginning
    auto it = std::rbegin(container);
    // Before adding to list we must create sexp pointer
    obj3 = sexp_make_cpointer(ctx, sexp_type_tag(obj1), &(*it), SEXP_FALSE, 0);
    objList = sexp_cons(ctx, obj3, SEXP_NULL);

    while (it != std::rend(container))
    {
        obj3 = sexp_make_cpointer(ctx, typeTag, &*it, SEXP_FALSE, 0);
        objList = sexp_cons(ctx, obj3, objList);
        ++it;
    }

    // Make sure our newly created list is not deleted
    sexp_preserve_object(ctx, objList);
    sexp_gc_release3(ctx);
    return objList;
}

vec2 myVector{10.f, 10.f};

auto genRandList()
{
    using namespace std;
    random_device rd;
    mt19937 engineMt(rd());
    uniform_real_distribution<float> unif50(0.f, 50.f);
    uniform_int_distribution<int> uniform_int_100(0, 100);
    auto random_50 = bind(unif50, engineMt);
    auto random_100_int = bind(uniform_int_100, engineMt);
    static const unsigned sizeVec{ 10 };
    
    vector<vec2> myList(sizeVec);
    auto genVec2 = [&](){ return vec2{(float)random_100_int(), (float)random_100_int()}; };
    generate(begin(myList), end(myList), genVec2);
    return myList;
    
}


void dostuff(sexp ctx)
{
	using namespace std;

    // Declare and preserve local variables
    sexp_gc_var3(obj1, obj2, obj3);
    sexp_gc_preserve3(ctx, obj1, obj2, obj3);
   

    printVector(myVector);

    std::string str_change_vector = R"(
        (define (change-vec v)
          (begin (vec2-x! v (+ (vec2-x v) 20.0))
          (vec2-y! v 20.0))
         )
   
    )";

    std::string str_xx = R"(
    
    (define (nth n l)
     (if (or (> n (length l)) (< n 0))
      '()
      (if (eq? n 0)
       (car l)
       (nth (- n 1) (cdr l)))))
    
    )";

    // create a pointer to C struct
    obj1 = sexp_c_string(ctx, "vec2", -1);
    obj1 = sexp_lookup_type(ctx, obj1, SEXP_FALSE);
    obj3 = sexp_make_cpointer(ctx, sexp_type_tag(obj1), &myVector, SEXP_FALSE, 0);
    
    // define change-vec function
    obj2 = sexp_eval_string(ctx, str_change_vector.c_str(), -1, NULL);
    obj2 = sexp_eval_string(ctx, str_xx.c_str(), -1, NULL);
    
    obj2 = sexp_intern(ctx, "change-vec", -1);
    //obj1 = sexp_cons(ctx, obj3, SEXP_NULL);
    //obj1 = sexp_cons(ctx, obj2, obj1);
    // same as above
    obj1 = sexp_list2(ctx, obj2, obj3);
    sexp_eval(ctx, obj1, NULL);
    
    printVector(myVector, "Now my vector: ");
    
    // create global?
    obj2 = sexp_intern(ctx, "my-vec", -1);
    sexp_env_define(ctx, sexp_context_env(ctx), obj2, obj3);
    sexp_preserve_object(ctx, obj3);
    
    obj2 = sexp_eval_string(ctx, "(change-vec my-vec)", -1, NULL);
    printVector(myVector, "Now my vector: ");
    
    // Eval a C string as Scheme code
    obj1 = sexp_eval_string(ctx, "(+ 2 2)", -1, NULL);
    //obj2 = sexp_eval_string(ctx, "(so-long)", -1, NULL);
    auto x = sexp_unbox_fixnum(obj1);
    std::cout << x << std::endl; 

    // Release the local variables
    sexp_gc_release3(ctx);
}


int main()
{
    sexp ctx;
    ctx = sexp_make_eval_context(NULL, NULL, NULL, 0, 0);
    sexp_load_standard_env(ctx, NULL, SEXP_SEVEN);
    sexp_load_standard_ports(ctx, NULL, stdin, stdout, stderr, 0);
    
    // Registering our C functions
    sexp_init_library(ctx, NULL, 3, sexp_context_env(ctx), sexp_version, SEXP_ABI_IDENTIFIER);
    dostuff(ctx);

    auto myList = genRandList();
    auto l = buildList(ctx, myList);
    // Create global
    auto obj3 = sexp_intern(ctx, "my-list", -1);
    sexp_env_define(ctx, sexp_context_env(ctx), obj3, l);
    
    
    char buffer[512];
    //char response[1024];
    std::string response;
    while (1)
    {
        std::cout << std::endl << "chibi> ";
        fgets(buffer, 512, stdin);
        if ((buffer[0] != '\n') || 
        (strlen(buffer) > 1))
        {                            
            //sprintf(response, "%s", buffer);
            std::cin >> response;
            // lightweight embedded repl with custom input/output
            auto ret1 = sexp_eval_string(ctx, response.c_str(), -1, NULL);
            auto ret2 = sexp_write_to_string(ctx, ret1);
            auto ret3 = sexp_string_data(ret2);
            //auto ret = sexp_read_from_string(ctx, response, -1);
            std::cout << ret3 << std::endl;
        }
    }

    sexp_destroy_context(ctx);
}
