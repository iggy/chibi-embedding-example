#! /usr/bin/env python
# encoding: utf-8
# Thomas Nagy, 2006-2010 (ita)

# the following two variables are used by the target "waf dist"
VERSION='0.0.1'
APPNAME='chibi_test'

# these variables are mandatory ('/' are converted automatically)
top = '.'
out = 'build'

def options(opt):
	opt.load('compiler_cxx')
        opt.load('compiler_c')
        
def configure(conf):
        conf.load('compiler_cxx')   	
        conf.load('compiler_c')   
#        conf.env.append_value('INCLUDES', ['external/chibi-scheme/include'])
#        conf.env.append_value('INCLUDES', ['external/chibi-scheme/include', 'external/chibi-scheme/include/chibi'])
        conf.env.append_value('CCFLAGS', '-std=c++14 -Wall ')
        conf.env.append_value('INCLUDES', ['chibi/include'])
        conf.check(
                features='cxx cxxprogram', 
                cxxflags=['-std=c++14', '-Wall'],
        )
#        conf.env.append_value('INCLUDES', ['chibi/include', 'chibi/include/chibi'])                
#        conf.check(header_name='stdio.h', features='cxx cxxprogram', mandatory=False)

def build(bld):
#	bld.shlib(source='a.cpp', target='mylib', vnum='9.8.7')
#	bld.shlib(source='a.cpp', target='mylib2', vnum='9.8.7', cnum='9.8')
#	bld.shlib(source='a.cpp', target='mylib3')


#        s1 = bld.path.ant_glob('m2.cpp')
        s1 = bld.path.ant_glob('*.cpp')
        print(s1)
	bld.program(source=s1,
                    features='cxx cxxprogram',
                    target='app',
                    cxxflags     = ['-std=c++14', '-ldl'],
                    use='chibi'
        )
#	bld.program(source=s1, target='app', use='chibi')        
        ss = bld.path.ant_glob('chibi/*.c')
        print(ss)
	bld.stlib(target='chibi',
                  source=ss,
                  cflags='-ldl'
        )
#       	bld.stlib(target='chibi', source=ss)

	# just a test to check if the .c is compiled as c++ when no c compiler is found
#	bld.program(features='cxx cxxprogram', source='main.c', target='app2')

	if bld.cmd != 'clean':
		from waflib import Logs
		bld.logger = Logs.make_logger('test.log', 'build') # just to get a clean output
		#bld.check(header_name='sadlib.h', features='cxx cxxprogram', mandatory=False)
		bld.logger = None

